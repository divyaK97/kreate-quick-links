# Kreate Quick Links

1. There are different portals where each portal is dedicated for a specific task. This menu bar aims to bring all those portals under one master page.


* HRMS -> 
        Leave Management System

* JIRA -> 
        Complete Project Management System

* Activity Entry-> 
        Daily Reporting to the manager about what you did each day, and what are your plans for next month.

* Klass/ Percipio -> 
        Skill enhancement platforms

* Empower Genie -> 
        Platform to share certificates collected by you and know the skills you must have to go up the ladder

* Red Mine ->
        Software used by testers to open bugs and share feedback
